import {LIST_SUCCESS, LIST_REQUEST, LIST_ERROR} from "./action";

const initialState = {
    arrayList: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LIST_REQUEST:
            return {
                ...state,
                loading: true
            };
        case LIST_SUCCESS:
            alert('Success');
            return {
                ...state,
                arrayList: action.listData,
                loading: false
            };
        case LIST_ERROR:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
};

export default reducer;