import axios from 'axios';

export const LIST_REQUEST = 'LIST_REQUEST';
export const LIST_SUCCESS = 'LIST_SUCCESS';
export const LIST_ERROR = 'LIST_ERROR';

export const listRequest = () => {
  return {type: LIST_REQUEST}
};

export const listSuccess = (listData) => {
  return {type: LIST_SUCCESS, listData}
};

export const listError = () => {
    return {type: LIST_ERROR}
};

export const getList = () => {
    return dispatch => {
        dispatch(listRequest());
        axios.get('https://www.reddit.com/r/pics.json').then(response => {
            console.log(response.data.data.children);
            dispatch(listSuccess(response.data.data.children));
        }, error => {
            dispatch(listError());
        })
    }
};