import React, {Component} from 'react';
import {StyleSheet, FlatList, Text, View} from 'react-native';
import {connect} from 'react-redux';

import ListItem from '../../components/ListItem/ListItem';
import {getList} from "../../store/action";

class List extends Component {

    componentDidMount() {
        this.props.getList();
    }

    render() {

        if (this.props.loading) {
            return (
                <View style={styles.loading}>
                    <Text>Loading...</Text>
                </View>
            )
        } else {
            return (
                <FlatList
                    style={styles.container}
                    data={this.props.arrayList}
                    keyExtractor={(item) => item.data.id}
                    renderItem={(info) => (
                        <ListItem
                            text={info.item.data.title}
                            img={info.item.data.thumbnail}
                        />
                    )}
                />
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        marginVertical: 30
    },
    loading: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapToProps = state => {
    return {
        arrayList: state.arrayList,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getList: () => dispatch(getList())
    }
};

export default connect(mapToProps, mapDispatchToProps)(List);